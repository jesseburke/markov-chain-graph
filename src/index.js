import React from 'react';
import ReactDOM from 'react-dom';
import 'katex/dist/katex.min.css';
import TeX from '@matejmazur/react-katex';



import {matrix_factory, identity_matrix_factory} from './matrix.js';
import './index.css';

const start_data = {matrix:[[.1,.8],[.9,.2]], vec:[.9,.1], show_ssv:true};

ReactDOM.render(<App matrix={start_data.matrix} vec={start_data.vec}
		show_ssv={start_data.show_ssv}/>,
		document.getElementById("main-div"));   

//----------------------------------------
//
// expects:
// props.matrix
//
//----------------------------------------

function App(props) {
    const [matrix, set_matrix] = React.useState(props.matrix);
    const [vec_v, set_vec_v] = React.useState(props.vec);
    const [n_value, set_n_value] = React.useState(1)
    const [show_ssv, set_show_ssv] = React.useState(props.show_ssv);

    function matrix_change_funct(col,value) {
	// if value is bigger than one, set the other editable entry
	// in that col to zero
	if(value > 1) {
	    value = 1;
	    set_matrix(prev_mat =>
		       set_entry_of_matrix(prev_mat,1,col,0));
	}
	
	// if value is less than zero, set the other editable entry
	// in that col to one
	if(value < 0) {
	    value = 0;	

	    set_matrix(prev_mat =>
		       set_entry_of_matrix(prev_mat,1,col,1));				
	}

	set_matrix(prev_mat =>
		   set_entry_of_matrix(prev_mat,0,col,value));
	set_matrix(prev_mat =>
		   set_entry_of_matrix(prev_mat,1,col,1-value));

	set_show_ssv(false);
    }

    function vec_change_func(e) {
	let value = e.target.value;
	if(value < 0) {
	    set_vec_v([value,1]);
	    return;
	}

	set_vec_v([value,1-value]);
    }

    function ssv_click_funct() {
	set_show_ssv(!(show_ssv));
    }

    const style = {width:150, margin:30};
    
    return (<div className="main-div">
	    <div className="input-block">
	    <div className="input-comp">	   
	    <div> <TeX>A = </TeX><ProbMatrix matrix={matrix} 
	    on_change_funct={matrix_change_funct}/>
	    </div>
	    <input type="button" value ="Show steady state prob."
	    onClick={ssv_click_funct}/>
	    <SteadyStateVector show={show_ssv} matrix={matrix}/>	   
	    </div>
	    <div className="input-comp">
	    <div className="vertical-vector">
	    <TeX>v = </TeX><div><table><tbody><tr>
	    <td><input type="text" size="4" value={vec_v[0]}
	    onChange={vec_change_func}/></td>
	    </tr>
	    <tr>
	    <td>{round(vec_v[1])}</td>
	    </tr>
	    </tbody>
	    </table>
	    </div>
	    </div>
	    <div className="vertical-vector">	   
	    <TeX>{String.raw`A^{${n_value}}v=`}</TeX>
	    <table><tbody>
	    <tr>
	    <td>{round(matrix_factory(matrix).multiply_with_vec(vec_v, n_value)[0])}</td>
	    </tr>
	    <tr>
	    <td>{round(matrix_factory(matrix).multiply_with_vec(vec_v, n_value)[1])}</td>
	    </tr>
	    </tbody>
	    </table>
	    </div>	   
	    <div style={style}>
	    <input name="n" type="range" value={n_value} step={1}
	    onChange={(e) => set_n_value(e.target.value)}
	    min={1} max={20}
	    />
	    <label for="range_n">n={n_value}</label>
	    </div>
	    </div>
	    </div>
	    <div className="math-plane">
	    <Plane data={{matrix,n_value,vec_v,show_ssv}}/>
	    </div>
	    </div>
	   );
}

 // <InputRange value={n_value} onChange={(value) => set_n_value(value)}
 // 	    formatLabel={value => `n=${value}`} minValue={1} maxValue={20}
 // 	    />


//----------------------------------------
// the following transforms coordinates...
//
//----------------------------------------
const xSize = 550;
const ySize = 550;

const grid_placement_const = .09;

const xMin = -grid_placement_const;
const xMax = 1 + grid_placement_const;
const yMin = -grid_placement_const;
const yMax = 1 + grid_placement_const;

function canvas_from_xy([x, y]) {
    var xPerc = (x-xMin)/(xMax-xMin);
    var yPerc = (y-yMin)/(yMax-yMin);
    return ([(xPerc * xSize), (ySize - yPerc * ySize)]);
}
//----------------------------------------

const ssv_color = '#ef8261';
const vec_color = '#934940';
const pt_color = '#d24749';


function Plane(props) {
    const canvasRef = React.useRef(null);
    const A = matrix_factory(props.data.matrix);
    const vec = props.data.vec_v;
    const n = props.data.n_value;

    React.useEffect( () => {
	const ctx = canvasRef.current.getContext('2d');
	ctx.font = "20px Arial";
	ctx.clearRect(0, 0, xSize, ySize);

	ctx.strokeStyle = '#000';
	ctx.fillStyle = '#000';
	ctx.lineWidth   = 1;
	ctx.globalAlpha=.7;
	draw_axes(ctx);
	draw_grid(ctx);
	ctx.globalAlpha=1;
	
	ctx.strokeStyle = vec_color;
	ctx.fillStyle = vec_color;
	ctx.lineWidth   = 5;
	draw_vector(ctx, A.multiply_with_vec(vec,n));
	
	
	if(props.data.show_ssv) {
	    ctx.strokeStyle = ssv_color;
	    ctx.fillStyle = ssv_color;
	    ctx.lineWidth   = 5;
	    let v = steady_state_vector(props.data.matrix);
	    draw_vector(ctx, v);
	    let text_coords = canvas_from_xy([v[0],v[1]]);
	    ctx.fillText("ssvp",text_coords[0]+10,text_coords[1]-10);
	}

	for (let i = 1; i < n; i++) {
	    ctx.strokeStyle = pt_color;
	    ctx.fillStyle = pt_color;
	    ctx.lineWidth   = 3;
	    ctx.globalAlpha = .3;
	    draw_point(ctx,A.multiply_with_vec(vec,i), 10);
	    ctx.globalAlpha = 1;
	}
	
    });

    function draw_axes(ctx, ticks = 3, ) {
	const left_point = canvas_from_xy([.7*xMin,0]);
	const right_point = canvas_from_xy([.95*xMax,0]);
	const bot_point = canvas_from_xy([0,.7*yMin]);
	const top_point = canvas_from_xy([0,.95*yMax]);
	
	//ctx.globalAlpha=.6;
	ctx.beginPath();
	ctx.moveTo(left_point[0],left_point[1]);
	ctx.lineTo(right_point[0],right_point[1]);
	ctx.moveTo(bot_point[0],bot_point[1]);
	ctx.lineTo(top_point[0],top_point[1]);
	ctx.stroke();
	ctx.closePath();

	canvas_arrow(ctx,bot_point[0],bot_point[1],top_point[0],top_point[1],7);
	ctx.fillText("y",top_point[0]-25,top_point[1]+15);
	canvas_arrow(ctx,left_point[0],left_point[1],right_point[0],right_point[1],7);
	ctx.fillText("x",right_point[0]-10,right_point[1]+25);
    }

    function draw_grid(ctx) {

    }

    // draws vector from origin to (x,y)
    function draw_vector(ctx,  [x,y]) {
	const start = canvas_from_xy([0,0]);
	const end = canvas_from_xy([x,y]);
		
	ctx.beginPath();
	ctx.moveTo(start[0],start[1]);
	ctx.lineTo(end[0],end[1]);	
	ctx.stroke();
	ctx.fill();
	ctx.closePath();
	
	canvas_arrow(ctx, start[0], start[1], end[0], end[1],10);		
    }

    // taken from https://stackoverflow.com/questions/808826/draw-arrow-on-canvas-tag
    function canvas_arrow(context, fromx, fromy, tox, toy, r=10){
	let x_center = tox;
	let y_center = toy;

	let angle;
	let x;
	let y;

	context.beginPath();
	angle = Math.atan2(toy-fromy,tox-fromx)
	x = r*Math.cos(angle) + x_center;
	y = r*Math.sin(angle) + y_center;

	context.moveTo(x, y);

	angle += (1/3)*(2*Math.PI)
	x = r*Math.cos(angle) + x_center;
	y = r*Math.sin(angle) + y_center;

	context.lineTo(x, y);

	angle += (1/3)*(2*Math.PI)
	x = r*Math.cos(angle) + x_center;
	y = r*Math.sin(angle) + y_center;

	context.lineTo(x, y);

	context.fill();
	context.closePath();
    }

    function draw_point(ctx, [x,y], rad=5) {
	const center = canvas_from_xy([x,y]);
	
	ctx.beginPath();
	ctx.arc(center[0],center[1],rad,0,2*Math.PI);	
	ctx.fill();
	ctx.closePath();
    }

    return <canvas
    ref={canvasRef}
    width="550"
    height="550"   
	/>;
}

// function that takes a 2x2 probability matrix and returns the steady state vector
function steady_state_vector(matrix) {
    let m = matrix_factory(matrix).add(identity_matrix_factory(2).scalar_multiply(-1));
    let ev = m.basis_null_space()[0];

    let sum = ev[0]+ev[1];
    return [ev[0]/sum, ev[1]/sum];
}

// expects props.matrix; assumes it has eigenvalue 1
// calculates and displays unit eigenvector of eigenvalue 1
function SteadyStateVector(props) {   
    if(props.show) {
	let ssv = steady_state_vector(props.matrix);
	
	return <div className="vertical-vector">sspv = <table><tbody><tr>
	    <td>{round(ssv[0])}</td></tr>
	    <tr><td>{round(ssv[1])}</td></tr>
	    <tr></tr></tbody></table></div>;
    }

    return <div className="vector-display-phantom">sspv = <table><tbody><tr>
	<td>{round(1/3)}</td></tr>
	<tr><td>{round(1/3)}</td></tr>	  
	<tr></tr></tbody></table></div>;
}



//----------------------------------------
//
// expects:
// props.matrix,
// props.change_function (this function expects arguments i and j)
//
//----------------------------------------
function ProbMatrix(props) {

    function make_table_cell(i,j) {
	return (<td><input type="text" size="6" value={(props.matrix[i])[j]}
		onChange={(e) => handle_change(e,i,j)}/></td>);
    }

    function handle_change(e,i,j) {
	props.on_change_funct(j,e.target.value);
    }

    function make_table_row(i) {
	let new_row = [0,1];

	return new_row.map(j => make_table_cell(i,j));
    }

    return (<table>
	    <tbody>	    
	    <tr>
	    {make_table_row(0)}
	    </tr>
	    <tr>	    
	    <td>{round(1-props.matrix[0][0])}</td>
	    <td>{round(1-props.matrix[0][1])}</td>
	    </tr>
	    </tbody>
	    </table>
	   );
}


//----------------------------------------
//
// set_entry_of_matrix
//
// input: matrix, array of arrays
//        i,j, value
//
//
// output: new matrix, array of arrays
//
//----------------------------------------
function set_entry_of_matrix(matrix, i,j,value) {
    let new_arr = [];
    for( let row = 0; row < matrix.length; row++) {
	new_arr[row]=[];
	for( let col = 0; col < matrix[row].length; col++) {
	    if(i === row && j === col) {new_arr[row][col]=value;}
	    else {new_arr[row][col]=matrix[row][col];}
	}
    }

    return new_arr;
}

function get_entry_of_matrix(matrix, i,j) {  
    return matrix[i][j];
}

// not sure about this
function round(num,n=5) {
    return (Math.round((10**n)*num+.00001)/10**n);
}

export default App;
