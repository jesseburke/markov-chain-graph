markov-chain-graph

Running at http://www.maths.usyd.edu.au/u/jburke/markov-graph/index.html

To compile, clone the repo, and in that folder, run "npm install" and then "npm start". This will open a local server at http://localhost:3000/ where you will see the app.